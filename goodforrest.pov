/*file for declarations of trees and good forrest*/

/*
macro parameters are:
	xcoord is number of x to move 
	zcoord is number of z to move
	sizeOfBush is size of sphere on top of tree (shouldbe betwen .1 and .9)
	inColour is colour of top of tree
*/
/*macro for creating a tall tree*/
#macro Make_Tall_Tree(Xcoord, Zcoord, sizeOfBush, inColour)
	union
	{
		cylinder
		{
			<0, -1, 0>,
			<0, 1, 0> .1
			pigment {color DarkWood}
			normal {wrinkles 1.25 scale .1}
			translate <Xcoord, 0, Zcoord>
		}
		sphere
		{
			<-0, 1.25, 0> sizeOfBush
			pigment {color inColour transmit .2}
			translate <Xcoord, 0, Zcoord>
		}
	}
#end

/*macro for creating a medium sized tree*/
#macro Make_Medium_Tree(Xcoord, Zcoord, sizeOfBush, inColour)
	union
	{
		cylinder
		{
			<0, -1, 0>,
			<0, .75, 0> .1
			pigment {color DarkWood}
			normal {wrinkles 1.25 scale .1}
			translate <Xcoord, 0, Zcoord>
		}
		sphere
		{
			<0, 1, 0> sizeOfBush
			pigment {color inColour transmit .1}
			translate <Xcoord, 0, Zcoord>
		}
	}
#end

/*macro for creating a short tree*/
#macro Make_Short_Tree(Xcoord, Zcoord, sizeOfBush, inColour)
	union
	{
		cylinder
		{
			<0, -1, 0>,
			<0, .5, 0> .1
			pigment {color DarkWood}
			normal {wrinkles 1.25 scale .1}
			translate <Xcoord, 0, Zcoord>
		}
		sphere
		{
			<0, .75, 0> sizeOfBush
			pigment {color inColour transmit .1}
			translate <Xcoord, 0, Zcoord>
		}
	}
#end

/*create object goodForrest*/
	//stubs
		/* 
		object
		{
			Make_Short_Tree ( , , , )
		}

		object
		{
			Make_Medium_Tree ( , , , )
		}

		object
		{
			Make_Tall_Tree ( , , , )
		}
		*/
#declare goodForrest = union
{

	/* Forrest 1*/
	/*
		Short: 	8
		Medium: 6
		Tall:	6
	*/
	/*can not use loop as do not know
	how to change colour of tree with
	each iteration*/

	/*tree 1 short LimeGreen*/
	object
	{
		Make_Short_Tree (-.5, 3, .4, LimeGreen)
	}

	/*tree 2 tall Blue*/
	object
	{
		Make_Tall_Tree (-1.3, 3.4, .7, Blue)
	}

	/*tree 3 small red*/
	object
	{
		Make_Short_Tree (-2, 4, .4, Red)
	}

	/*tree 4 tall pink*/
	object
	{
		Make_Tall_Tree (0, 4, .4, SpicyPink)
	}

	/*tree 5 tall SeaGreen*/
	object
	{
		Make_Tall_Tree (-.2, 4.5, .7, SeaGreen)
	}

	/*tree 6 tall Aquamarine*/
	object
	{
		Make_Tall_Tree (-1.2, 5, .4, Aquamarine)
	}

	/*tree 7 tall green*/
	object
	{
		Make_Tall_Tree (-2, 5.5, .6, Green)
	}

	/*tree 8 Medium NeonBlue*/
	object
	{
		Make_Medium_Tree (-1, 6, .7, NeonBlue)
	}

	/*tree 9 Medium MediumGoldenrod*/
	object
	{
		Make_Medium_Tree (-1.9, 6.5, .6, MediumGoldenrod)
	}

	/*tree 10 short Plum*/
	object
	{
		Make_Short_Tree (-1.8, 7.5, .5, Plum)
	}

	/*tree 11 short Cyan*/
	object
	{
		Make_Short_Tree (-.5, 7.5, .5, Cyan)
	}

	/*tree 12 short IndianRed*/
	object
	{
		Make_Short_Tree (-1, 8.3, .5, IndianRed)
	}

	/*tree 13 short OldGold*/
	object
	{
		Make_Short_Tree (-.5, 9, .6, OldGold)
	}

	/*tree 14 short ForestGreen*/
	object
	{
		Make_Short_Tree (-1.8, 9, .6, ForestGreen)
	}

	/*tree 15 short FeldSpar*/
	object
	{
		Make_Short_Tree (-1.4, 10, .5, Feldspar)
	}

	/*tree 16 Medium SummerSky*/
	object
	{
		Make_Medium_Tree (-.8, 10, .5, SummerSky)
	}

	/*tree 17 Medium */
	object
	{
		Make_Medium_Tree (-.1, 10.5, .2, Scarlet)
	}

	/*tree 18 Medium */
	object
	{
		Make_Medium_Tree (-1.2, 10.8, .6, RichBlue)
	}

	/*tree 19 Medium */
	object
	{
		Make_Medium_Tree (-1.8, 11.5, .4, DustyRose)
	}

	/*tree 20 Tall */
	object
	{
		Make_Tall_Tree (-.4, 11, .7, Quartz)
	}


	/* Forrest 2*/
	/*
		Short: 	8
		Medium: 6
		Tall:	6
	*/
	/*tree 1 short LimeGreen*/
	object
	{
		Make_Short_Tree (-3.5, 3, .4, LimeGreen)
	}

	/*tree 2 tall Blue*/
	object
	{
		Make_Tall_Tree (-4.3, 3.4, .7, Blue)
	}

	/*tree 3 small red*/
	object
	{
		Make_Short_Tree (-5, 4, .4, Red)
	}

	/*tree 4 tall pink*/
	object
	{
		Make_Tall_Tree (-3, 4, .4, SpicyPink)
	}

	/*tree 5 tall SeaGreen*/
	object
	{
		Make_Tall_Tree (-3.2, 4.5, .7, SeaGreen)
	}

	/*tree 6 tall Aquamarine*/
	object
	{
		Make_Tall_Tree (-4.2, 5, .4, Aquamarine)
	}

	/*tree 7 tall green*/
	object
	{
		Make_Tall_Tree (-5, 5.5, .6, Green)
	}

	/*tree 8 Medium NeonBlue*/
	object
	{
		Make_Medium_Tree (-4, 6, .7, NeonBlue)
	}

	/*tree 9 Medium MediumGoldenrod*/
	object
	{
		Make_Medium_Tree (-4.9, 6.5, .6, MediumGoldenrod)
	}

	/*tree 10 short Plum*/
	object
	{
		Make_Short_Tree (-4.8, 7.5, .5, Plum)
	}

	/*tree 11 short Cyan*/
	object
	{
		Make_Short_Tree (-2.8, 7.5, .5, Cyan)
	}

	/*tree 12 short IndianRed*/
	object
	{
		Make_Short_Tree (-4, 8.3, .5, IndianRed)
	}

	/*tree 13 short OldGold*/
	object
	{
		Make_Short_Tree (-3.5, 9, .6, OldGold)
	}

	/*tree 14 short ForestGreen*/
	object
	{
		Make_Short_Tree (-4.8, 9, .6, ForestGreen)
	}

	/*tree 15 short FeldSpar*/
	object
	{
		Make_Short_Tree (-4.4, 10, .5, Feldspar)
	}

	/*tree 16 Medium SummerSky*/
	object
	{
		Make_Medium_Tree (-3.8, 10, .5, SummerSky)
	}

	/*tree 17 Medium */
	object
	{
		Make_Medium_Tree (-3.1, 10.5, .2, Scarlet)
	}

	/*tree 18 Medium */
	object
	{
		Make_Medium_Tree (-4.2, 10.8, .6, RichBlue)
	}

	/*tree 19 Medium */
	object
	{
		Make_Medium_Tree (-4.8, 11.5, .4, DustyRose)
	}

	/*tree 20 Tall */
	object
	{
		Make_Tall_Tree (-3.4, 11, .7, Quartz)
	}


	/* Forrest 3*/
	/*
		Short: 	8
		Medium: 6
		Tall:	6
	*/
	/*tree 1 short LimeGreen*/
	object
	{
		Make_Short_Tree (-.5, 12, .4, LimeGreen)
	}

	/*tree 2 tall Blue*/
	object
	{
		Make_Tall_Tree (-1.3, 12.4, .7, Blue)
	}

	/*tree 3 small red*/
	object
	{
		Make_Short_Tree (-2, 13, .4, Red)
	}

	/*tree 4 tall pink*/
	object
	{
		Make_Tall_Tree (0, 13, .4, SpicyPink)
	}

	/*tree 5 tall SeaGreen*/
	object
	{
		Make_Tall_Tree (-.2, 13.5, .7, SeaGreen)
	}

	/*tree 6 tall Aquamarine*/
	object
	{
		Make_Tall_Tree (-1.2, 14, .4, Aquamarine)
	}

	/*tree 7 tall green*/
	object
	{
		Make_Tall_Tree (-2, 14.5, .6, Green)
	}

	/*tree 8 Medium NeonBlue*/
	object
	{
		Make_Medium_Tree (-1, 15, .7, NeonBlue)
	}

	/*tree 9 Medium MediumGoldenrod*/
	object
	{
		Make_Medium_Tree (-1.9, 15.5, .6, MediumGoldenrod)
	}

	/*tree 10 short Plum*/
	object
	{
		Make_Short_Tree (-1.8, 16.5, .5, Plum)
	}

	/*tree 11 short Cyan*/
	object
	{
		Make_Short_Tree (-.5, 16.5, .5, Cyan)
	}

	/*tree 12 short IndianRed*/
	object
	{
		Make_Short_Tree (-1, 17.3, .5, IndianRed)
	}

	/*tree 13 short OldGold*/
	object
	{
		Make_Short_Tree (-.5, 18, .6, OldGold)
	}

	/*tree 14 short ForestGreen*/
	object
	{
		Make_Short_Tree (-1.8, 18, .6, ForestGreen)
	}

	/*tree 15 short FeldSpar*/
	object
	{
		Make_Short_Tree (-1.4, 19, .5, Feldspar)
	}

	/*tree 16 Medium SummerSky*/
	object
	{
		Make_Medium_Tree (-.8, 19, .5, SummerSky)
	}

	/*tree 17 Medium */
	object
	{
		Make_Medium_Tree (-.1, 19.5, .2, Scarlet)
	}

	/*tree 18 Medium */
	object
	{
		Make_Medium_Tree (-1.2, 19.8, .6, RichBlue)
	}

	/*tree 19 Medium */
	object
	{
		Make_Medium_Tree (-1.8, 20.5, .4, DustyRose)
	}

	/*tree 20 Tall */
	object
	{
		Make_Tall_Tree (-.4, 20, .7, Quartz)
	}


	/*neutral row*/
	/*tree 1 Medium */
	object
	{
		Make_Medium_Tree (1.5, 3, .4, Green)
	}
	/*tree 2 Tall */
	object
	{
		Make_Tall_Tree (1.2, 4.4, .7, SpicyPink)
	}
	/*tree 3 short ForestGreen*/
	object
	{
		Make_Short_Tree (1.8, 6, .6, ForestGreen)
	}
	/*tree 4 Tall */
	object
	{
		Make_Tall_Tree (1.55, 7, .7, Aquamarine)
	}
	/*tree 5 Tall */
	object
	{
		Make_Tall_Tree (1.1, 8.5, .7, Blue)
	}
	/*tree 6 short ForestGreen*/
	object
	{
		Make_Short_Tree (1.8, 10, .6, Red)
	}
		/*tree 4 Tall */
	object
	{
		Make_Tall_Tree (1.55, 11.5, .7, Plum)
	}
	/*tree 5 Tall */
	object
	{
		Make_Tall_Tree (1.1, 13, .7, CoolCopper)
	}
	/*tree 6 short ForestGreen*/
	object
	{
		Make_Short_Tree (1.8, 14.5, .3, Khaki)
	}

}

