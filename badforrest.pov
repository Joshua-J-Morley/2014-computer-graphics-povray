/*create object DeadTree*/
#declare DeadTree = union
{
	cylinder
	{
		<0, -1, 0>,
		<0, 1.2, 0> .1
		pigment {color DarkWood}
		normal {wrinkles 1.25 scale .1}
		translate <0, 0, 2>
	}
	union
	{
		#local lengthCtrl = -.58;
		#local heightCtrl = .8;
		#while (lengthCtrl <= -.10)
			union
			{
				#local axisCtrl = -10;
				#while (axisCtrl <= 350)
					cylinder
					{
						<0, lengthCtrl, 0>,
						<0, 0, 0> .015
						pigment {color DarkWood}
						normal {wrinkles 1.25 scale .1}
						rotate<90, axisCtrl, 0>
						translate <0, heightCtrl, 2>
					}
					#local axisCtrl = axisCtrl + 20;
				#end
			}
			#local heightCtrl = heightCtrl + .1;
			#local lengthCtrl = lengthCtrl + .12;
		#end
	}
	union
	{
		#local lengthCtrl = -.46;
		#local heightCtrl = .8;
		#while (lengthCtrl <= -.10)
			union
			{
				#local axisCtrl = -10;
				#while (axisCtrl <= 350)
					cylinder
					{
						<0, lengthCtrl, 0>,
						<0, 0, 0> .015
						pigment {color DarkWood}
						normal {wrinkles 1.25 scale .1}
						rotate<90, axisCtrl, 0>
						translate <0, heightCtrl, 2>
					}
					#local axisCtrl = axisCtrl + 20;
				#end
			}
			#local heightCtrl = heightCtrl - .1;
			#local lengthCtrl = lengthCtrl + .12;
		#end
	}
	translate<2.2, 0, 0>
}

/*create object badRow = row of DeadTree*/
#declare badRow = union
{
	#local zAxis = 0;
	#while (zAxis < 14)
		union
		{
			object
			{
				DeadTree
				translate<0, 0, zAxis>
			}
		}
		#local zAxis = zAxis + 2;
	#end
}

/*create object badForrest = columns of BadRows*/
#declare badForrest = union
{
	object
	{
		badRow
	}
	object
	{
		badRow
		translate<1, 0, .50>
	}
	object
	{
		badRow
		translate<2, 0, .20>
	}
	object
	{
		badRow
		translate<3, 0, .90>
	}
	object
	{
		badRow
		translate<4, 0, 0>
	}
	object
	{
		badRow
		translate<5, 0, .50>
	}
	object
	{
		badRow
		translate<6, 0, .20>
	}
	object
	{
		badRow
		translate<7, 0, .90>
	}
}