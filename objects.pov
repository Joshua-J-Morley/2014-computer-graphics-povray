
/*create object Table*/
#declare Table = union
{
	/*table top*/
	box
	{
		<-1, .30, .9>,
		<-1.5, .2, 1.6>
		pigment
		{
			leopard scale .055
			color_map
			{
			    [0.000 Red]
			    [0.280 Yellow]
			    [0.5 Blue]
			    [0.7 Green]
			    [0.9 Pink]
			}
		}
		translate<.9, .02, -2.55>
	}
	/*Table leg*/
	cylinder
	{
		<-2, -1, 1>,
		<-2, 1, 1> .1
		pigment {color Brass}
		normal {wrinkles 1.25 scale .1}
		scale <.251, .31, 1>
		translate<0,0,-2.5>
	}
	/*Table leg*/
	cylinder
	{
		<-2, -1, 1>,
		<-2, 1, 1> .1
		pigment {color Brass}
		normal {wrinkles 1.25 scale .1}
		scale <.251, .31, 1>
		translate<.3,0,-2.5>
	}
	/*Table leg*/
	cylinder
	{
		<-2, -1, 1>,
		<-2, 1, 1> .1
		pigment {color Brass}
		normal {wrinkles 1.25 scale .1}
		scale <.251, .31, 1>
		translate<.3,0,-2>
	}
	/*Table leg*/
	cylinder
	{
		<-2, -1, 1>,
		<-2, 1, 1> .1
		pigment {color Brass}
		normal {wrinkles 1.25 scale .1}
		scale <.251, .31, 1>
		translate<0,0,-2>
	}
}


/*create object Cup*/
#declare Cup = lathe
{
	quadratic_spline
	5,
	<2, 0>,
  	<3, 0>,
  	<3, 5>,
 	<2, 5>,
  	<2, 0>
  	texture
  	{
    	pigment
    	{
    		color rgb<0.4,0.2,1>
    	}
    	finish 
    	{ 
    		phong 1 reflection 0.2
    	}
  	} // end of texture
  scale<.01,.01,.01>*1
  rotate<0,0,0>
}


/*create object Archway*/
#declare Archway = union
{
	torus
	{
		1, .015
		scale <.7, .2, 2>
		pigment {color rgb <1, 1, 1>}
		normal
		{
			leopard 15
			scale .055
		}
		rotate<90, 0, 0> 
		translate<.5, 0, 0>
	}
	torus
	{
		1, .015
		scale <.7, .2, 2>
		pigment {color rgb <1, 1, 1>}
		normal
		{
			leopard 15
			scale .055
		}
		rotate<90, 0, 0> 
		translate<.5, 0, .5>
	}
	torus
	{
		1, .015
		scale <.7, 18, 2>
		pigment {color rgb <1, 1, 1> transmit .7}
		normal
		{
			leopard 15
			scale .055
		}
		rotate<90, 0, 0> 
		translate<.5, 0, .25>
	}
}

/*create object BloodDrip*/
#declare BloodDrip = union
{
	cylinder
	{
		<6.700, 1, 15.5>,
		<6.7005, 1, 15.5> .08
		pigment {color Red}
		translate <-.05, .17, .159>

	}
	cylinder
	{
		<6.700, 1, 15.5>,
		<6.7005, 1, 15.5> .08
		pigment {color Red}
		translate <-.05, .175, 0.009>
	}
	cylinder
	{
		<6.700, 1, 15.5>,
		<6.7005, 1, 15.5> .08
		pigment {color Red}
		translate <-.05, .175, -.059>
	}
}