/* include povray files*/
#include "colors.inc"
#include "shapes.inc"
#include "textures.inc"

/*include my files*/
#include "objects.pov"
#include "goodforrest.pov"
#include "badforrest.pov"
global_settings { max_trace_level 8 }

/*colour declarations*/
#declare my_Red = color rgbf <1 ,.2 ,.2 ,1 >;
#declare my_Orange = color rgbf <1 ,.5 ,.2 ,1 >;
#declare my_Yellow = color rgbf <1 ,1 ,.2 ,1 >;
#declare my_Green = color rgbf <.2 ,1 ,.2 ,1 >;
#declare my_LightBlue = color rgbf <.2 ,1 ,1 ,1 >;
#declare my_Blue = color rgbf <.2 ,.2 ,1 ,1 >;
#declare my_Purple = color rgbf <.5 ,.5 ,1 ,1 >;
#declare my_Pink = color rgbf <1 ,.5 ,1 ,1 >;

/* Cameras */
#if (clock <= 16)
	camera
	{
		location <.5, 1, -5>
		look_at <.5, 1, 0>
		#if (clock <= 16)
			translate <0, 0, clock * 1.2>
		#end
	}
#end
#if ((clock >= 17) & (clock<= 20))
	#declare ElseClock = clock - 16;
	camera
	{
		location <.5, 1, 15.5>
		look_at <5, 1, 15.5>
		translate < ElseClock *1.2, 0, 0>
		//end position <6.5, 1, 15.5>
	}
#end
#if ((clock >=21) & (clock <= 22))
	camera
	{
		location <6.5, 1, 15.5>
		look_at <10, 1, 15.5>
	}
	object
	{
		BloodDrip
		translate <0, -(clock/500), 0>
	}
#end
#if ((clock >=23) & (clock <= 25))
	camera
	{
		location <6.5, 1, 15.5>
		look_at <10, 1, 15.5>
	}
	object
	{
		BloodDrip
		translate <0, -(clock/200), 0>
	}
#end
#if (clock >= 26)
	camera
	{
		location <16.5, 1, 15.5>
		look_at <18.5, 1, 15.5>
		translate < (clock/100) *1.2, 0, 0>
		//end position <6.5, 1, 15.5>
	}
	box
	{
		<18, 0, 18.5>,
		<110, 4, 10.5>
		pigment { color Red }
		normal
		{
			bump_map
			{
 				png "plasma3.png"
			}
		}
	}
#end

/*camera birds eye view*/
/*camera
{
	location <-1, 22, 8>
	look_at <-1, 0, 8>
}
*/

/*lights*/
light_source
{
	<-100, 100, -150>
	#if (clock <= 10)
		color White
	#else
		color Red
	#end
}
light_source
{
	<-10, 10, 0>
	color White
}
light_source
{
	<5, 1, 4>
	color Red
}

/*background objects*/
background
{
	color SkyBlue
}

/*rainbow*/
rainbow
{
	angle 30
	width 2
	arc_angle 120
	falloff_angle 50
	distance 10000000
	direction <-.2, -.2, 1>
	jitter .01
	color_map
	{
		[0.000 color my_Red transmit 0.8]
	    [0.160 color my_Orange transmit 0.8]
	    [0.284 color my_Yellow transmit 0.8]
	    [0.408 color my_Green transmit 0.8]
	    [0.522 color my_LightBlue transmit 0.8]
	    [0.646 color my_Blue transmit 0.8]
	    [0.760 color my_Purple transmit 0.8]
	    [0.884 color my_Pink transmit 0.8]
	}
}




/*'good' side*/
/*floor*/
box
{
	<2, 0, -20>,
	<-200, -1.5, 200>
	pigment 
	{
		hexagon
		color Red
		color Green
		color Blue
		scale .25
	}
}

/*footpath*/
box
{
	<.9, 0, -6>,
	<0.1, .05, 15>
	pigment 
	{
		brick
		Gold,
		Yellow
		scale .0135
	}
}
box
{
	<.1, 0, 15>,
	<2, .05, 15.8>
	pigment 
	{
		brick
		Gold,
		Yellow
		scale .0135
	}
}

/*Colourful Pillar/building*/
box
{
	<0, 0, 20>,
	<2, 4, 17>
	pigment
	{
		wood
		color_map
		{
		    [0.000 Red]
		    [0.280 Yellow]
		    [0.5 Blue]
		    [0.7 Green]
		    [0.9 Pink]
		}
	}
}

/*protective 'sandbox'*/
object
{
	Supertorus(1, .25, 2, .45, .001, 1.5)
	texture
	{
		pigment {Brown}
	}

	scale <1, 1, 1>
	translate <-2, 0, 1>
}
/*suppport column*/
cylinder
{
	<-2, -1, 1>,
	<-2, 1, 1> .1
	pigment {color Brass}
	normal {wrinkles 1.25 scale .1}
}
/*force field*/
sphere
{
	<-2, 1.25, 1> .5
	pigment {color NeonPink transmit .3}
}

/*Table*/
object
{
	Table
}
//Cup 1
object
{
	Cup
	translate<-.4,.31,-1.2>
}
//Cup 2
object
{
	Cup
	translate<-.3,.31,-1.2>
}

/*archway*/
object
{
	Archway
	translate<0, 0, -2.5>
}

/*good forrest*/
object
{ 
	goodForrest
}




/*evil side*/
/*evil sky*/
box
{
	<2, 0, 200>,
	<250, 200, 200>
	pigment 
	{
    	crackle scale 1.5 turbulence .35
    	color_map 
    	{
    		[0.2 Red]
        	[0.4 DarkWood]
        	[0.6 Firebrick]
        	[1 VioletRed]
    	}
    }
}

/*evil floor*/
box
{
	<2, 0, -20>,
	<200, -1.5, 200>
	pigment {Brown}
	normal
	{
		agate .5
		scale 1
	}
}

/*footpath*/
box
{
	<2, 0, 15>,
	<15, .05, 15.8>
	texture
	{
		pigment 
		{
			brick
			HuntersGreen
			DarkWood
			scale .0135
		}
		normal { bumps 0.3 scale 0.2}
	}
}

/*dark pillar/building*/
box
{
	<2, 0, 17>,
	<4, 4, 20>
	pigment 
	{
    	crackle scale 1.5 turbulence .35
    	color_map 
    	{ 
	    	[0 DarkTan transmit .3]
        	[0.4 Black transmit .2]
        	[1 Grey transmit .5]
    	}
    }
}

/*evil forrest*/
object
{
	badForrest
}

/*single dead tree 
created to demonstrate 
code maniupulation*/
/*
object
{
	DeadTree
	translate <-3, 0, -3>
}
*/